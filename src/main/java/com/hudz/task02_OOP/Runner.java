package com.hudz.task02_OOP;

import com.hudz.task02_OOP.views.View;
/**
 * The class with function main.
 */
public class Runner {
    /**
     * Entry point
     * @param args command line parameters
     */
    public static void main(String[] args) {

            View view = new View();
            view.start();
    }
}
