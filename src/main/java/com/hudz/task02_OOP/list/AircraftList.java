package com.hudz.task02_OOP.list;

import com.hudz.task02_OOP.models.aircrafts.Aircraft;
import com.hudz.task02_OOP.models.aircrafts.CargoPassengerPlane;
import com.hudz.task02_OOP.models.aircrafts.CargoPlane;
import com.hudz.task02_OOP.models.aircrafts.PassengerPlane;

import java.util.ArrayList;

/**
 * The class with list of some aircraft
 */

public class AircraftList {

    /**
     * The static method for returning Array List with values
     */
    public static ArrayList<Aircraft> addAircraft() {

        ArrayList<Aircraft> aircrafts = new ArrayList<Aircraft>();

        aircrafts.add(0,  new CargoPlane("Company0", "737", 7000, 1000.00, 9000));
        aircrafts.add(1,  new CargoPassengerPlane("Company1","24", 5000, 3200, 567, 38));
        aircrafts.add(2,  new PassengerPlane("Company3", "24", 8000, 3200, 78));
        aircrafts.add(3,  new PassengerPlane("Company4", "777", 2000, 3200, 78));
        aircrafts.add(4,  new CargoPassengerPlane("Company5", "888", 1000, 3200, 567, 38));

        return aircrafts;
    }
}
