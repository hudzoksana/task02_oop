package com.hudz.task02_OOP.views;

import java.util.ArrayList;
import com.hudz.task02_OOP.list.AircraftList;
import com.hudz.task02_OOP.models.aircrafts.Aircraft;
import com.hudz.task02_OOP.сontroller.AirlneController;
import com.hudz.task02_OOP.сontroller.IOController;
/**
 * The class with simple user interface.
 */
public class View {

    AirlneController airlneController = new AirlneController();
    /**
     * The method to start simple user interface
     */
    public void start() {

        String airlineName;
        ArrayList<Aircraft> aircraft = AircraftList.addAircraft();

        System.out.println("Please enter name for the NEW airline:");
        airlineName = IOController.getString();
        /* creating of new airline */
        airlneController.createAirline(airlineName,aircraft);
        System.out.println("\nIn your airline " + airlneController.getNameAirline()
                +  " already are planes:\n" + airlneController.getAircraft());
        /* printing max cargo and max amount of passengers */
        System.out.println("\nSum of cargo capacity is " + airlneController.gettingMaxCargo());
        System.out.println("Max amount of passengers is " + airlneController.gettingMaxPassengers());
        /* printing sorted by flight range list of aircraft */
        System.out.println("\nAfter sorting by fuel reange we will get:\n" +
                            airlneController.resultSortingAircraftFlightRange());
        /* printing list of airline's aircraft in range od fuel capacity */
        searchingFuelCapacity();
    }

    public void searchingFuelCapacity () {

        int paramMin;
        int paramMax;

        System.out.print("\nNow we will search a plane with fuel capacity in an interval!" +
                           "\n\nPlease enter minimum capacity for searching: ");
        paramMin = IOController.getNumber();
        System.out.print("\nPlease enter maximum capacity for searching: ");
        paramMax = IOController.getNumber();
        /* returning array list with founded aircraft */
        System.out.println(airlneController.searchingAircraftByFuelCapacity(paramMin,paramMax));
    }
}
