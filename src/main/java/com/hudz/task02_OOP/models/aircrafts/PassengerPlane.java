package com.hudz.task02_OOP.models.aircrafts;
/**
 * The class for extending Aircraft with Passengers aircraft
 */
public class PassengerPlane extends Aircraft {

    private final int seatNo;

    public PassengerPlane(final String company,
                          final String model,
                          final double flightRange,
                          final double fuelCapacity,
                          final int seatNo) {

        super(company, model, flightRange, fuelCapacity);
        this.seatNo = seatNo;
    }

    @Override
    public final double getCargoCapacity() {
        return 0;
    }

    @Override
    public final int getSeatingCapacity() {
        return seatNo;
    }

    @Override
    public final String toString() {
        return "\nPassengers Plane  {"
                + "seatCapacity = " + this.getSeatingCapacity()
                + "\tcargoCapacity = " + this.getCargoCapacity()
                + "\tflightRange= " + this.getFlightRange()
                + "\tflightRange =  " + this.getFuelCapacity()
                +'}';
    }
}
