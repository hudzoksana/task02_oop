package com.hudz.task02_OOP.models.aircrafts;
/**
 * The class for extending Aircraft with Cargo aircraft
 */
public class CargoPlane extends Aircraft {

    private final Double cargoCapacity;

    public CargoPlane(final String company,
                      final String model,
                      final double flightRange,
                      final double fuelCapacity,
                      final double cargoCapacity) {

        super(company, model, flightRange, fuelCapacity);
        this.cargoCapacity = cargoCapacity;
    }

    @Override
    public final double getCargoCapacity() {
        return cargoCapacity;
    }

    @Override
    public final int getSeatingCapacity() {
        return 0;
    }

    @Override
    public final String toString() {
        return "\nCargo Plane {"
                + "seatCapacity = " + this.getSeatingCapacity()
                + "\tcargoCapacity = " + this.getCargoCapacity()
                + "\tflightRange =  " + this.getFlightRange()
                + "\tflightRange =  " + this.getFuelCapacity()
                +'}';
    }
}
