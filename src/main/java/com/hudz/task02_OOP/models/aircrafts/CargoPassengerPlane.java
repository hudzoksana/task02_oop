package com.hudz.task02_OOP.models.aircrafts;
/**
 * The class for extending Aircraft with Cargo and Passengers aircraft
 */
public class CargoPassengerPlane extends Aircraft {

    private Double cargoCapacity;
    private int seatNo;

    public CargoPassengerPlane(final String company,
                               final String model,
                               final double flightRange,
                               final double fuelCapacity,
                               final double cargoCapacity,
                               final int seatNo) {

        super(company, model, flightRange, fuelCapacity);
        this.cargoCapacity = cargoCapacity;
        this.seatNo = seatNo;
    }

    @Override
    public final double getCargoCapacity() {
        return cargoCapacity;
    }

    @Override
    public final int getSeatingCapacity() {
        return seatNo;
    }

    @Override
    public final String toString() {
        return "\nCargo Passenger Plane  {"
                + "seatCapacity = " + this.getSeatingCapacity()
                + "\tcargoCapacity = " + this.getCargoCapacity()
                + "\tflightRange =  " + this.getFuelCapacity()
                +'}';
    }
}
