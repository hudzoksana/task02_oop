package com.hudz.task02_OOP.models.aircrafts;
import java.util.Comparator;

/**
 * The class for comparing two aircraft.
 */
public class AircraftComparator implements Comparator<Aircraft> {
    /**
     * The method for comparing two aircraft by flight range.
     * @param a1 first aircraft
     * @param a2 second aircraft
     * @return result of comparing two flights by flight range
     */
    public final int compare(Aircraft a1, Aircraft a2) {
        return new Double(a1.getFlightRange()).compareTo(a2.getFlightRange());
    }
}
