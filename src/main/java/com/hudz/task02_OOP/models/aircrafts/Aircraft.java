package com.hudz.task02_OOP.models.aircrafts;
/**
 * Abstract Factory for Aircraft
 */
public abstract class Aircraft {

    private final String company;
    private final String model;
    private final double flightRange;
    private final double fuelCapacity;

    /**
     * The Airplane constructor is responsible for creating a new airplane
     * @param company name of the manufacturer
     * @param model model of the plane
     * @param flightRange aircraft's flight range
     * @param fuelCapacity aircraft's fuel capacity
     */
    public Aircraft(String company,
                    String model,
                    double flightRange,
                    double fuelCapacity) {

        this.company = company;
        this.model = model;
        this.flightRange = flightRange;
        this.fuelCapacity = fuelCapacity;
    }
    /**
     * The abstract method for getting amount of seats in aircraft
     */
    public abstract int getSeatingCapacity();

    /**
     * The abstract method for getting cargo capacity of aircraft
     */
    public abstract double getCargoCapacity();

    /**
     * The method for getting flight range of aircraft
     * @return flight range
     */
    public final double getFlightRange() {
        return flightRange;
    }
    /**
     * The method for getting fuel capacity of aircraft
     * @return fuel capacity of aircraft
     */
    public final double getFuelCapacity() {
        return fuelCapacity;
    }
    /**
     * The method for getting company manufacturer of aircraft
     * @return name of company
     */
    public final String getCompany() {
        return company;
    }
    /**
     * The method for getting model of aircraft
     * @return name of the aircraft`s model
     */
    public final String getModel() {
        return model;
    }
    /**
     * The abstract method for printing info about aircraft
     */
    @Override
    public abstract String toString();

}
