package com.hudz.task02_OOP.models;

import com.hudz.task02_OOP.models.aircrafts.Aircraft;
import com.hudz.task02_OOP.models.aircrafts.AircraftComparator;
import java.util.ArrayList;
import java.util.Collections;

/**
 * The class with airline model
 */
public class Airline  {

    private String nameAirline;
    private ArrayList<Aircraft> aircraft = new ArrayList<Aircraft>();

    /**
     * The constructor for creating airline
     * @param aircraft for list of aircraft which belongs to this airline
     * @param nameAirline for name of a new airline
     */
    public Airline(final String nameAirline,
                   final ArrayList<Aircraft> aircraft) {

        this.nameAirline = nameAirline;
        this.aircraft = aircraft;
    }
    /**
     * The method for adding new airline`s aircraft
     * @param craft object Aircraft
     */
    public final void addAircraft(final Aircraft craft) {
        aircraft.add(craft);
    }
    /**
     * The method for getting airline`s name
     */
    public final String getNameAirline() {

        return nameAirline;
    }
    /**
     * The method for setting airline`s name
     */
    public final void setNameAirline(final String nameAirline) {

        this.nameAirline = nameAirline;
    }
    /**
     * The method for getting Aircraft list of airline
     */
    public final ArrayList<Aircraft> getAircraft() {

        return aircraft;
    }
    /**
     * The method for setting Aircraft list of airline
     */
    public final void setAircraft(final ArrayList<Aircraft> aircraft) {
        this.aircraft = aircraft;
    }
    /**
     * A method for searching plane by range of fuel capacity
     * @param airline object Airline
     * @param min set by user value of minimum value in interval of fuel capacity for searching
     * @param max set by user value of maximum value in interval of fuel capacity for searching
     * @return Array List with aircraft which fuel capacity is between min and max
     */
    public final ArrayList<Aircraft> searchingAircraftByFuelCapacity(final Airline airline,
                                                                     final int min,
                                                                     final int max) {
        ArrayList<Aircraft> resultList = new ArrayList<Aircraft>();

        for (int i = 0; i < airline.getAircraft().size(); ++i) {

            double planeFuelCapacity = airline.getAircraft().get(i).getFuelCapacity();
            if (planeFuelCapacity >= min && planeFuelCapacity <= max) {
                resultList.add(airline.getAircraft().get(i));
            }
        }
        return resultList;
    }
    /**
     * A method for sorting aircraft of airline by flight range
     * @param airline object Airline
     */
    public final ArrayList<Aircraft> sortingAircraftFlightRange(Airline airline) {
        Collections.sort(airline.getAircraft(), new AircraftComparator());
        return airline.getAircraft();
    }
    /**
     * A method to counting maximum cargo capacity
     * @return cargo capacity of all aircraft which belongs to the airline
     */
    public final double calculateTotalCargoCapacity() {
        int totalCargoCapacity = 0;
        for (int i = 0; i < this.getAircraft().size(); i++) {
            totalCargoCapacity += this.getAircraft().get(i).getCargoCapacity();
        }
        return totalCargoCapacity;
    }

    /**
     * A method to counting maximum amount of seats.
     * @return amount of seats at all aircraft which belongs to the airline
     */
    public final int calculateTotalPassengerCapacity() {
        int totalPassengerCapacity = 0;
        for (int i = 0; i < this.getAircraft().size(); i++) {
            totalPassengerCapacity += this.getAircraft().get(i).getSeatingCapacity();
        }
        return totalPassengerCapacity;
    }

}
