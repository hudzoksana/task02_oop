package com.hudz.task02_OOP.сontroller;

import com.hudz.task02_OOP.models.Airline;
import com.hudz.task02_OOP.models.aircrafts.Aircraft;
import java.util.ArrayList;
/**
 * The class with functions for working with aircraft model.
 */
public class AirlneController {

    Airline airline;
    /**
     * A method for working with sorting aircraft of airline by flight range
     */
    public final String resultSortingAircraftFlightRange() {
        ArrayList<Aircraft> aircraft = airline.sortingAircraftFlightRange(airline);
        if (!aircraft.isEmpty()) {
            return aircraft.toString();
        } else return "Sorry the list of airline's aircraft is empty!";
    }

    public final Airline createAirline(String name, ArrayList<Aircraft> aircrafts) {
        return airline = new Airline(name, aircrafts);
    }

    public final String getNameAirline() {
        return this.airline.getNameAirline();
    }

    public final ArrayList<Aircraft> getAircraft() {
        return this.airline.getAircraft();
    }
    /**
     * A method for searching plane by range of fuel capacity
     * @param min set by user value of minimum value in interval of fuel capacity for searching
     * @param max set by user value of maximum value in interval of fuel capacity for searching
     * @return Array List with aircraft which fuel capacity is between min and max
     */
    public final String searchingAircraftByFuelCapacity(final int min, final int max) {
        ArrayList<Aircraft> searchedAircraft =
                airline.searchingAircraftByFuelCapacity(airline, min, max);
        if(!searchedAircraft.isEmpty()) {
            return "\n\nLook what we found:\n" + searchedAircraft;
        } else return "Sorry no matches!";
    }

    public final double gettingMaxCargo() {

        if(airline.getAircraft().isEmpty()) {
            return 0;
        } else return airline.calculateTotalCargoCapacity();
    }

    public final int gettingMaxPassengers() {

        if(airline.getAircraft().isEmpty()) {
            return 0;
        } else return airline.calculateTotalPassengerCapacity();
    }
}
