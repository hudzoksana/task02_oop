package com.hudz.task02_OOP.сontroller;

import java.util.Scanner;

/**
 * The class with function for input/output information
 */
public class IOController {

    /**
     * The method for getting int from the console
     * @return number from command line
     */
    public static int getNumber() {

        int number;
        Scanner sc = new Scanner(System.in);
        number = sc.nextInt();

        return number;
    }
    /**
     * The method for getting String from the console
     * @return String value
     */
    public static String getString() {

        String name;
        Scanner sc = new Scanner(System.in);
        name = sc.next();

        return name;
    }
}
